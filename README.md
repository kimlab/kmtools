# kmtools

[![anaconda](https://img.shields.io/conda/dn/kimlab/kmtools.svg)](https://anaconda.org/kimlab/kmtools/)
[![docs](https://img.shields.io/badge/docs-v0.2.9-blue.svg?version=latest)](https://kimlab.gitlab.io/kmtools/v0.2.9/)
[![pipeline status](https://gitlab.com/kimlab/kmtools/badges/v0.2.9/pipeline.svg)](https://gitlab.com/kimlab/kmtools/commits/v0.2.9/)
[![coverage report](https://gitlab.com/kimlab/kmtools/badges/v0.2.9/coverage.svg)](https://kimlab.gitlab.io/kmtools/v0.2.9/htmlcov/)

Bits of reusable code to make our lives easier.

Follows the _Whole Sort of General Mish Mash_ design principle.

- [kmtools](#kmtools)
  - [Contents](#contents)
    - [Database tools](#database-tools)
    - [DataFrame tools](#dataframe-tools)
    - [Machine learning tools](#machine-learning-tools)
    - [Python tools](#python-tools)
    - [Science tools](#science-tools)
    - [Sequence tools](#sequence-tools)
    - [Structure tools](#structure-tools)
    - [System tools](#system-tools)
  - [Contributing](#contributing)

## Contents

### Database tools

### DataFrame tools

### Machine learning tools

### Python tools

### Science tools

### Sequence tools

### Structure tools

- Using [kmbio](https://github.com/kimlaborg/kmbio) instead of [biopython](https://github.com/biopython/biopython) leads to substantially better performance (> 2x with lots of room for improvement).

### System tools

## Contributing

- Make sure all tests pass before merging into master.
- Follow the PEP8 / PyFlake / Flake8 / etc. guidelines.
- Add tests for new code.
- Try to document things.
- Break any / all of the above if you have a good reason.
