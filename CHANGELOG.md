# Change Log

## Version 0.0.23

- Update code for calculating residue-residue interactions both for residues on the same chain and for residues on different chains. Improve test coverage for this code.

## Version 0.0.18
