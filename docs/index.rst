========
Contents
========

.. toctree::
   :maxdepth: 2
   :glob:

   readme
   installation
   usage
   modules
   authors
   history
   environment_variables.md
   examples/index.rst
   api/generated/kmtools

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
